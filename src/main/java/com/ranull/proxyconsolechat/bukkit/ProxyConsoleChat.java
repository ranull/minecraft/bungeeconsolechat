package com.ranull.proxyconsolechat.bukkit;

import com.ranull.proxyconsolechat.bukkit.listener.AsyncPlayerChatListener;
import com.ranull.proxyconsolechat.bukkit.manager.ChatManager;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.java.JavaPlugin;

public class ProxyConsoleChat extends JavaPlugin {
    private ChatManager chatManager;

    @Override
    public void onEnable() {
        chatManager = new ChatManager(this);

        registerListeners();
        registerChannels();
    }

    @Override
    public void onDisable() {
        unregisterListeners();
        unregisterChannels();
    }

    public void registerListeners() {
        getServer().getPluginManager().registerEvents(new AsyncPlayerChatListener(this), this);
    }

    public void unregisterListeners() {
        HandlerList.unregisterAll(this);
    }

    public void registerChannels() {
        getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
    }

    public void unregisterChannels() {
        getServer().getMessenger().unregisterOutgoingPluginChannel(this);
    }

    public ChatManager getChatManager() {
        return chatManager;
    }
}